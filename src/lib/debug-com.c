/*
 * This file is part of the coreboot project.

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <rules.h>
#include <stdint.h>
#include <console/uart.h>
#include <console/usb.h>
#include <debug-com.h>

#define CONFIG_UART_FOR_GDB 1
#define USB_PIPE_FOR_GDB 0

#define CONFIG_UART_FOR_SERIALICE 1
#define USB_PIPE_FOR_SERIALICE 0

/* Configure functions to use GDB arguments */
#if __CONSOLE_USB_ENABLE__ && CONFIG_GDB_STUB

static inline void __debug_hw_init(void) { usbdebug_init(); }
static inline void __debug_tx_byte(u8 data)
	{ usb_tx_byte(USB_PIPE_FOR_GDB, data); }
static inline void __debug_tx_flush(void)
	{ usb_tx_flush(USB_PIPE_FOR_GDB); }
static inline u8 __debug_rx_byte(void)
	{ return usb_rx_byte(USB_PIPE_FOR_GDB); }

#elif __CONSOLE_SERIAL_ENABLE__ && CONFIG_GDB_STUB

static inline void __debug_hw_init(void)
	{ uart_init(CONFIG_UART_FOR_GDB); }
static inline void __debug_tx_byte(u8 data)
	{ uart_tx_byte(CONFIG_UART_FOR_GDB, data); }
static inline void __debug_tx_flush(void)
	{ uart_tx_flush(CONFIG_UART_FOR_GDB); }
static inline u8 __debug_rx_byte(void)
	{ return uart_rx_byte(CONFIG_UART_FOR_GDB); }

/* Configure functions to use SerialICE arguments */
#elif __CONSOLE_USB_ENABLE__ && CONFIG_DEBUG_SERIALICE

static inline void __debug_hw_init(void) { usbdebug_init(); }
static inline void __debug_tx_byte(u8 data)
	{ usb_tx_byte(USB_PIPE_FOR_SERIALICE, data); }
static inline void __debug_tx_flush(void)
	{ usb_tx_flush(USB_PIPE_FOR_SERIALICE); }
static inline u8 __debug_rx_byte(void)
	{ return usb_rx_byte(USB_PIPE_FOR_SERIALICE); }

#elif __CONSOLE_SERIAL_ENABLE__ && CONFIG_DEBUG_SERIALICE

static inline void __debug_hw_init(void)
	{ uart_init(CONFIG_UART_FOR_SERIALICE); }
static inline void __debug_tx_byte(u8 data)
	{ uart_tx_byte(CONFIG_UART_FOR_SERIALICE, data); }
static inline void __debug_tx_flush(void)
	{ uart_tx_flush(CONFIG_UART_FOR_SERIALICE); }
static inline u8 __debug_rx_byte(void)
	{ return uart_rx_byte(CONFIG_UART_FOR_SERIALICE); }

#else

static inline void __debug_hw_init(void)	{}
static inline void __debug_tx_byte(u8 data)	{}
static inline void __debug_tx_flush(void)	{}
static inline u8 __debug_rx_byte(void)	{ return 0; }

#endif


void debug_hw_init(void)
{
	__debug_hw_init();
}

void debug_tx_byte(unsigned char byte)
{
	__debug_tx_byte(byte);
}

void debug_tx_flush(void)
{
	__debug_tx_flush();
}

unsigned char debug_rx_byte(void)
{
	return __debug_rx_byte();
}

