/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2009 University of Heidelberg
 * Written by Mondrian Nuessle <nuessle@uni-heidelberg.de> for Univ. Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/* PILOT Super I/O is only based on LPC observation done on factory system. */


#include <arch/io.h>
#include <console/console.h>
#include <device/pnp.h>
#include "pilot.h"

void pilot_early_init(pnp_devfn_t dev)
{
	u16 port = dev >> 8;

	printk(BIOS_DEBUG, "Using port: %04x\n", port);
	pilot_disable_serial(PNP_DEV(port, 0x1));
	printk(BIOS_DEBUG, "disable serial 1\n");

	pnp_enter_ext_func_mode(dev);
	pnp_set_logical_device(PNP_DEV(port, 0x0));
	pnp_write_config(PNP_DEV(port, 0x0), 0x21, 0x1);
	pnp_exit_ext_func_mode(dev);

	pnp_enter_ext_func_mode(dev);
	pnp_set_logical_device(PNP_DEV(port, 0x1));
	pnp_set_enable(dev, 0);
	pnp_set_iobase(dev, PNP_IDX_IO0, 0x60);
	pnp_set_iobase(dev, PNP_IDX_IO1, 0x64);
	pnp_set_irq(dev, PNP_IDX_IRQ0, 1);
	pnp_set_irq(dev, PNP_IDX_IRQ1, 12);
	pnp_set_enable(dev, 1);
	pnp_exit_ext_func_mode(dev);

	pnp_enter_ext_func_mode(dev);
	pnp_set_logical_device(PNP_DEV(port, 0x8));
	pnp_set_enable(dev, 0);
	pnp_set_iobase(dev, PNP_IDX_IO0, 0x02f8);
	pnp_set_irq(dev, PNP_IDX_IRQ0, 3);
	pnp_set_enable(dev, 1);
	pnp_exit_ext_func_mode(dev);

}
