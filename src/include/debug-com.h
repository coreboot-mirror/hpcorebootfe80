/*
 * This file is part of the coreboot project.

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _DEBUG_COM_H_
#define _DEBUG_COM_H_

#include <stdint.h>

void debug_hw_init(void);
void debug_tx_byte(unsigned char byte);
void debug_tx_flush(void);
unsigned char debug_rx_byte(void);

#endif /* _DEBUG_COM_H_ */
